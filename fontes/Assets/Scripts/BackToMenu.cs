﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackToMenu : MonoBehaviour {

    public Text TitleApresentation;

	void Start ()
    {
		if(LevelController.victory == 'S')
        {
            TitleApresentation.gameObject.SetActive(true);
            TitleApresentation.text = "Você ganhou!";
        }
        else if(LevelController.victory == 'N')
        {
            TitleApresentation.gameObject.SetActive(true);
            TitleApresentation.text = "Você perdeu";
        }
        else
        {
            TitleApresentation.gameObject.SetActive(false);
        }
	}
}
