﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovment : MonoBehaviour {

    public float MinSpeed = 2;
    public float MaxSpeed = 4;
    float speed = 2;

    private void Start()
    {
        speed = Random.Range(MinSpeed, MaxSpeed);
        SetSpeedLevel();
    }

    private void Update()
    {
        float amtToMove = speed * Time.deltaTime;
        transform.Translate(Vector3.forward * amtToMove, Space.World);
    }

    private void SetSpeedLevel()
    {
        speed += LevelController.Level;
    }

    public void StopMoving()
    {
        this.speed = 0;
    }
}
