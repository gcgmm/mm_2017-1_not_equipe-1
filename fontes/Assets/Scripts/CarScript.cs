﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarScript : MonoBehaviour {

    private bool hit = false;

    public bool getHit()
    {
        return this.hit;
    }

    public void Hitting()
    {
        this.hit = true;
    }
}
