﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMovment : MonoBehaviour {

    public float speed = 2;

    void Start()
    {
        SetSpeedLevel();
    }

    void FixedUpdate()
    {
        float amtToMove = speed * Time.deltaTime;
        transform.Translate(Vector3.forward * amtToMove, Space.World);
        if (transform.position.z >= 15)
        {
            this.gameObject.GetComponent<Obstacles>().DestroyAllObstacles();
            this.gameObject.GetComponent<Obstacles>().InstantiateObstacles();
            this.gameObject.GetComponent<Obstacles>().InstantiateCars();
            this.gameObject.GetComponent<Letters>().InstantiateLetters();
            transform.position = new Vector3(transform.position.x, transform.position.y, -30);
        }
    }

    private void SetSpeedLevel()
    {
        speed += LevelController.Level;
    }

    public void StopMoving()
    {
        this.speed = 0;
    }
}
