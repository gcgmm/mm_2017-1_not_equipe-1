﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleScript : MonoBehaviour {

    private bool fall = false;

    public bool getFall()
    {
        return this.fall;
    }

    public void Falling()
    {
        this.fall = true;
    }
}
