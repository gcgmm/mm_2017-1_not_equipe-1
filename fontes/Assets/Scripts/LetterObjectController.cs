﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterObjectController : MonoBehaviour {

    public GameObject UIController;
    private char letterChar = 'D';

    private void Update()
    {
        //transform.Rotate(Vector3.right * Time.deltaTime * 20);
        //transform.Rotate(Vector3.forward * Time.deltaTime * 10);
    }

    public void SetUIControllerObject(GameObject UIControllerObj)
    {
        this.UIController = UIControllerObj;
    }

    public void SetLetterChar(char letter)
    {
        this.letterChar = letter;
    }

    private void OnTriggerEnter(Collider other)
    {
        GetLetter();
    }

    private void GetLetter()
    {
        this.UIController.transform.GetComponent<UIController>().LetterGot(letterChar);
        Destroy(this.gameObject);
    }
}
