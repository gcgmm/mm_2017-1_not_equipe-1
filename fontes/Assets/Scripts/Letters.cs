﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Letters : MonoBehaviour {

    public List<Texture> Textures;
    public int NumberOfLettersPerTile = 3;
    public GameObject LettersObject;

    public GameObject UIController;

    private Material newMaterial;
    private char chosenChar;

    public void InstantiateLetters()
    {
        for (int i = 0; i < NumberOfLettersPerTile; i++)
        {
            List<char> charsAvailable = UIController.transform.GetComponent<UIController>().GetAvailableChars();

            if(charsAvailable.Count > 0)
            {
                chosenChar = charsAvailable[(Random.Range(0, charsAvailable.Count))];
                InstantiateLetter(chosenChar);
            }
        }
        //instancia letra aleatória
        List<char> charsAlphabet = GetAlphabet();
        chosenChar = charsAlphabet[(Random.Range(0, charsAlphabet.Count))];
        InstantiateLetter(chosenChar);
    }

    private void InstantiateLetter(char vChosenChar)
    {
        GameObject letter = Instantiate(LettersObject) as GameObject;

        newMaterial = letter.transform.GetComponent<Renderer>().material;
        newMaterial.mainTexture = GetCurrentCharTexture(vChosenChar);

        letter.transform.GetComponent<Renderer>().material = newMaterial;
        letter.transform.GetComponent<LetterObjectController>().SetLetterChar(vChosenChar);
        letter.transform.GetComponent<LetterObjectController>().SetUIControllerObject(UIController);
        letter.transform.parent = this.transform;
        setPosition(letter);
    }

    private Texture GetCurrentCharTexture(char letter)
    {
        foreach (Texture texture in Textures)
        {
            string tName = texture.name;
            char[] charTName = tName.ToCharArray();
            if(charTName[0] == letter)
            {
                return texture;
            }
        }
        return null;
    }

    private List<char> GetAlphabet()
    {
        List<char> Alphabet = new List<char>();
        foreach (Texture texture in Textures)
        {
            string tName = texture.name;
            char[] charTName = tName.ToCharArray();
            Alphabet.Add(charTName[0]);
        }
        return Alphabet;
    }

    private void setPosition(GameObject letter)
    {
        float x = Random.Range(-0.75f, 0.75f);
        float y = Random.Range(0.9f, 1.5f);
        float z = Random.Range(-1.5f, 1.5f);
        letter.transform.localPosition = new Vector3(x, y, z);
    }
}
