﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    string palavra = "Dificuldade";
    private char[] letters;
    private ArrayList lettersController = new ArrayList();
    private List<UILetter> UILetterList = new List<UILetter>();

    public GameObject Grid;
    public GameObject LetterPrefab;
    public LevelController levelController;

	void Start ()
    {
        if (LevelController.Words != null)
        {
            palavra = LevelController.Words[LevelController.Level - 1];
        }
        
        palavra = palavra.ToUpper();
        letters = palavra.ToCharArray();
        for (int i = 0; i < letters.Length; i++)
        {
            lettersController.Add(false);
            buildGrid(letters[i]);

            UILetterList[i].SetLetterToView(true);
        }
	}
	
    public void LetterGot(char letterChar)
    {
        bool charExistInWord = false;
        for (int i = 0; i < letters.Length; i++)
        {
            if (letters[i] == letterChar)
            {
                charExistInWord = true;
                if (!UILetterList[i].GetAlredyGot())
                {
                    UILetterList[i].SetLetterToView();
                    VerifyEndOfLevel();
                    break;
                }
            }
        }
        if (!charExistInWord)
        {
            foreach (UILetter letter in UILetterList)
            {
                if (letter.GetAlredyGot())
                {
                    letter.LoseLetter();
                    break;
                }
            }
        }
    }

    private void VerifyEndOfLevel()
    {
        bool endOfLevel = true;
        foreach (UILetter letter in UILetterList)
        {
            if (!letter.GetAlredyGot())
            {
                endOfLevel = false;
            }
        }

        if (endOfLevel)
        {
            levelController.WinLevel();
        }
    }

    public List<char> GetAvailableChars()
    {
        List<char> availableChars = new List<char>();
         
        for (int i = 0; i < letters.Length; i++)
        {
            if (!UILetterList[i].GetAlredyGot())
            {
                availableChars.Add(letters[i]);
            }   
        }

        return availableChars;
    }

    private void buildGrid(char letterChar)
    {
        GameObject letter = Instantiate(LetterPrefab) as GameObject;
        UILetterList.Add(letter.transform.GetComponent<UILetter>());
        letter.transform.GetComponent<UILetter>().SetLetter(letterChar);
        letter.transform.parent = Grid.transform;
    }
}
