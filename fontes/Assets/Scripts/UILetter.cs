﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILetter : MonoBehaviour {

    private char letter;
    private bool alredyGot = false;

    public void SetLetter(char letterChar)
    {
        this.letter = letterChar;
    }

    public char GetLetter()
    {
        return this.letter;
    }

    public void SetLetterToView(bool start = false)
    {
        if (start)
            this.gameObject.transform.GetComponentInChildren<Text>().text = this.letter.ToString();
        else
        {
            this.alredyGot = true;
            this.gameObject.transform.GetComponentInChildren<Text>().color = Color.red;
        }
    }

    public void LoseLetter()
    {
        this.alredyGot = false;
        this.gameObject.transform.GetComponentInChildren<Text>().color = Color.black;
    }

    public bool GetAlredyGot()
    {
        return this.alredyGot;
    }
}
